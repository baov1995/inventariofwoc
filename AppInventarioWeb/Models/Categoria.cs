﻿using System;
using System.Collections.Generic;

namespace AppInventarioWeb.Models
{
    public partial class Categoria
    {
        public Categoria()
        {
            Repuesto = new HashSet<Repuesto>();
        }

        public int CategoriaId { get; set; }
        public string Nombre { get; set; }

        public virtual ICollection<Repuesto> Repuesto { get; set; }
    }
}
