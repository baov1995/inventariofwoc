﻿using System;
using System.Collections.Generic;

namespace AppInventarioWeb.Models
{
    public partial class Pedido
    {
        public int PedidoId { get; set; }
        public string Nombre { get; set; }
        public string Cantidad { get; set; }
        public DateTime? FechaReserva { get; set; }
        public int? ClienteIdCliente { get; set; }
        public int? RepuestoIdRepuesto { get; set; }

        public virtual Cliente ClienteIdClienteNavigation { get; set; }
        public virtual Repuesto RepuestoIdRepuestoNavigation { get; set; }
    }
}
